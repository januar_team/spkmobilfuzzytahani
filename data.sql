/*
SQLyog Enterprise v10.42 
MySQL - 5.5.5-10.1.10-MariaDB-log : Database - spk_mobil
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`spk_mobil` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `spk_mobil`;

/*Table structure for table `cars` */

DROP TABLE IF EXISTS `cars`;

CREATE TABLE `cars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `merek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `jarak_tempuh` int(11) NOT NULL,
  `kapasitas_mesin` int(11) NOT NULL,
  `kapasitas_tangki` int(11) NOT NULL,
  `torsi_maks` double NOT NULL,
  `kapasitas_penumpang` int(11) NOT NULL,
  `bahan_bakar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transmisi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `cars` */

insert  into `cars`(`id`,`merek`,`nama`,`harga`,`tahun`,`jarak_tempuh`,`kapasitas_mesin`,`kapasitas_tangki`,`torsi_maks`,`kapasitas_penumpang`,`bahan_bakar`,`transmisi`) values (1,'TOYOTA','COROLLA TWINCAM 1.6 SE',47000000,1990,566977,1600,40,0.016,4,'premium','manual'),(2,'DAIHATSU','ESPASS',34000000,1996,290000,1500,35,0.039,7,'premium','manual'),(3,'SUZUKI','KARIMUN ESTILO',85000000,2010,37000,1100,40,0.024,4,'premium','manual'),(4,'DAIHATSU','NEW SIRION M/T',108000000,2011,2000,1300,40,0.026,5,'premium','manual');

/*Table structure for table `fuzzy_sets` */

DROP TABLE IF EXISTS `fuzzy_sets`;

CREATE TABLE `fuzzy_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fuzzy_variable_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `min_val` int(11) NOT NULL,
  `mid_val` int(11) NOT NULL,
  `max_val` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fuzzy_sets_fuzzy_variable_id_foreign` (`fuzzy_variable_id`),
  CONSTRAINT `fuzzy_sets_fuzzy_variable_id_foreign` FOREIGN KEY (`fuzzy_variable_id`) REFERENCES `fuzzy_variables` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `fuzzy_sets` */

insert  into `fuzzy_sets`(`id`,`fuzzy_variable_id`,`name`,`min_val`,`mid_val`,`max_val`) values (1,1,'Murah',0,50000000,100000000),(3,1,'Sedang',75000000,125000000,150000000),(4,1,'Mahal',135000000,200000000,0),(5,2,'Rendah',0,1990,2000),(6,2,'Sedang',1998,2003,2008),(7,2,'Tinggi',2005,2014,0),(8,3,'Dekat',0,100000,200000),(9,3,'Sedang',150000,280000,390000),(10,3,'Jauh',350000,450000,0);

/*Table structure for table `fuzzy_variables` */

DROP TABLE IF EXISTS `fuzzy_variables`;

CREATE TABLE `fuzzy_variables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `fuzzy_variables` */

insert  into `fuzzy_variables`(`id`,`name`,`description`) values (1,'Harga',''),(2,'Tahun',''),(3,'Jarak Tempuh',''),(4,'Kapasitas Mesin',''),(5,'Kapasitas Tangki',''),(6,'Torsi Maks',''),(7,'Kapasitas Penumpang','');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_11_07_113615_create_cars_table',2),(4,'2017_11_08_051400_create_fuzzy_variables_table',3),(5,'2017_11_09_205138_create_fuzzy_sets_table',4);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'Administrator','admin','admin@mail.com','$2y$10$MjmMrQpgaO7lZZIIMghYRucsL1KIiMQspNd03Fiv5BMxz0yt9iCMq','VROUzYjbifjNMYVXb85yRoBuDVQHF5S9DsR87IA5zzftIaV6INo2UQQFdVse','2017-11-06 18:47:35','2017-11-06 18:47:35');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
