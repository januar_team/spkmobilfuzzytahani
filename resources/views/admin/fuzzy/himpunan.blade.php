@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Fuzzy Variable
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Fuzzy Variable</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-list"></i> Fuzzy Variable</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody><tr>
                                            <th>Variable</th>
                                            <td>{{$variable->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Deskripsi</th>
                                            <td>{{$variable->description}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/admin/fuzzy/index" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-line-chart"></i> Himpunan Fuzzy</h3>

                        <div class="box-tools pull-right">
                            <a id="btn-add" href="#" class="btn btn-box btn-success"><i class="fa fa-plus"></i>
                                Add New
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="datatable" data-id="{{$variable->id}}" class="table table-bordered table-hover dataTable" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Nilai Min (a)</th>
                                            <th>Nilai Tengah (b)</th>
                                            <th>Nilai Max (c)</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="modal-add" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 id="form-title" class="modal-title">Add car</h4>
                </div>
                <form id="frm-add" class="">
                    <div class="modal-body">
                        <div class="alert alert-danger alert-dismissible" style="display: none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <span></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama" required>
                        </div>
                        <div class="form-group">
                            <label for="min_val">Nilai Min (a)</label>
                            <input type="number" step="any" class="form-control" id="min_val" name="min_val" placeholder="Nilai Min(a)" required>
                        </div>
                        <div class="form-group">
                            <label for="mid_val">Nilai Tengah (b)</label>
                            <input type="number" step="any" class="form-control" id="mid_val" name="mid_val" placeholder="Nilai Tengah (b)" required>
                        </div>
                        <div class="form-group">
                            <label for="max_val">Max Value (c)</label>
                            <input type="number" step="any" class="form-control" id="max_val" name="max_val" placeholder="Max Value (c)" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary btn-submit">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="modal-delete" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 id="form-title" class="modal-title">Delete car</h4>
                </div>
                <form id="frm-delete" class="">
                    <input type="hidden" name="id" id="id">
                    <div class="modal-body">
                        <p>Apakah Anda ingin menghapus data tersebut?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary btn-submit">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection

@section('script')
    <script type="application/javascript" src="/plugins/datatables/jquery.dataTables.js"></script>
    <script type="application/javascript" src="/plugins/datatables/dataTables.bootstrap.js"></script>
    <script type="application/javascript" src="/js/fuzzyset.js"></script>
@endsection