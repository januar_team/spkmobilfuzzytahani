@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Car List
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Car List</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-car"></i> Car List</h3>

                        <div class="box-tools pull-right">
                            <a id="btn-add" href="#" class="btn btn-box btn-success"><i class="fa fa-plus"></i>
                                Add New
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="datatable" class="table table-bordered table-hover dataTable" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th width="20px">#</th>
                                            <th>Merek</th>
                                            <th>Nama</th>
                                            <th>Harga</th>
                                            <th>Tahun</th>
                                            <th>Jarak Tempuh</th>
                                            <th>Kapasitas Mesin</th>
                                            <th>Kapasitas Tangki</th>
                                            <th>Torsi Maks</th>
                                            <th>Kapasitas Penumpang</th>
                                            <th>Bahan Bakar</th>
                                            <th>Transmisi</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="modal-add" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 id="form-title" class="modal-title">Add car</h4>
                </div>
                <form id="frm-add" class="">
                    <div class="modal-body">
                        <div class="alert alert-danger alert-dismissible" style="display: none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <span></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="merek">Merek</label>
                                    <input type="text" class="form-control" id="merek" name="merek" placeholder="Merek" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
                                </div>
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga" required>
                                </div>
                                <div class="form-group">
                                    <label for="tahun">Tahun</label>
                                    <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Tahun" required>
                                </div>
                                <div class="form-group">
                                    <label for="jarak_tempuh">Jarak tempuh</label>
                                    <input type="number" class="form-control" id="jarak_tempuh" name="jarak_tempuh" placeholder="Jarak tempuh" required>
                                </div>
                                <div class="form-group">
                                    <label for="kapasitas_mesin">Kapasitas mesin</label>
                                    <input type="number" class="form-control" id="kapasitas_mesin" name="kapasitas_mesin" placeholder="Kapasitas mesin" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="kapasitas_tangki">Kapasitas tangki</label>
                                    <input type="number" class="form-control" id="kapasitas_tangki" name="kapasitas_tangki" placeholder="Kapasitas tangki" required>
                                </div>
                                <div class="form-group">
                                    <label for="torsi_maks">Torsi maks</label>
                                    <input type="number" step="any" class="form-control" id="torsi_maks" name="torsi_maks" placeholder="Torsi maks" required>
                                </div>
                                <div class="form-group">
                                    <label for="kapasitas_penumpang">Kapasitas penumpang</label>
                                    <input type="number" class="form-control" id="kapasitas_penumpang" name="kapasitas_penumpang" placeholder="Kapasitas penumpang" required>
                                </div>
                                <div class="form-group">
                                    <label for="bahan_bakar">Bahan bakar</label>
                                    <select class="form-control" id="bahan_bakar" name="bahan_bakar" placeholder="Bahan bakar" required>
                                        <option value="premium">premium</option>
                                        <option value="pertalite">pertalite</option>
                                        <option value="pertamax">pertamax</option>
                                        <option value="pertamax plus">pertamax plus</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="transmisi">Transmisi</label>
                                    <select class="form-control" id="transmisi" name="transmisi" placeholder="Transmisi" required>
                                        <option value="manual">manual</option>
                                        <option value="automatic">automatic</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary btn-submit">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="modal-delete" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 id="form-title" class="modal-title">Delete car</h4>
                </div>
                <form id="frm-delete" class="">
                    <input type="hidden" name="id" id="id">
                    <div class="modal-body">
                        <p>Apakah Anda ingin menghapus data tersebut?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary btn-submit">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection

@section('script')
    <script type="application/javascript" src="/plugins/datatables/jquery.dataTables.js"></script>
    <script type="application/javascript" src="/plugins/datatables/dataTables.bootstrap.js"></script>
    <script type="application/javascript" src="/js/cars.js"></script>
@endsection