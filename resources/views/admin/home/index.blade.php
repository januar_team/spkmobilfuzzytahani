@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Home
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    {{--<div class="box-header with-border">--}}
                    {{--<h3 class="box-title">Dashboard</h3>--}}
                    {{--</div>--}}
                    <div class="box-body" style="text-align: center">
                        <H1>Selamat Datang</H1>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
