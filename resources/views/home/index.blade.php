@extends('layouts.user')

@section('content')
    <header class="masthead text-center text-white d-flex">
        <div class="container my-auto">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <h1 class="text-uppercase">
                        <strong>Temukan Kendaraan Sesuai Dengan Kriteria Anda</strong>
                    </h1>
                    <hr>
                </div>
                <div class="col-lg-8 mx-auto">
                    <p class="text-faded mb-5" style="background-color: rgba(0, 0, 0, 0.6);">Dengan memanfaatkan metode Fuzzy Tahani, kami mencoba melakukan
                    analisis untuk menentukan pilihan terbaik kendaraan bagi Anda sesuai dengan kriteria yang Anda inginkan</p>
                    <a class="btn btn-primary btn-xl js-scroll-trigger" href="/find">Find Car</a>
                </div>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="introduction">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <h2 class="section-heading text-white">We've got what you need!</h2>
                    <hr class="light my-4">
                    <p class="text-faded mb-4">
                        Salah satu hal yang harus diutamakan ketika memilih sebuah mobil adalah kegunaan mobil sesuai dengan kebutuhan, dimana setiap jenis mobil mempunyai kegunaan yang berbeda beda. Dengan begitu banyaknya jenis jenis mobil beserta spesifikasinya membuat para  konsumen  kesulitan  dalam  menentukan  atau  membeli  sebuah  mobil  karena banyak faktor yang perlu dipertimbangan dalam pengambilan keputusan.
                    </p>
                    <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Get Started!</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Perumusan Masalah</h2>
                    <hr class="my-4">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fa fa-4x fa-diamond text-primary mb-3 sr-icons"></i>
                        <h3 class="mb-3">Fuzzy model Tahani</h3>
                        <p class="text-muted mb-0">1.	Bagaimana menentukan fungsi keanggotaan yang akan digunakan pada perhitungan Fuzzy model Tahani untuk kasus pemilihan mobil bekas di PT Capella Daihatsu?</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fa fa-4x fa-paper-plane text-primary mb-3 sr-icons"></i>
                        <h3 class="mb-3">Logika query</h3>
                        <p class="text-muted mb-0">2.	Bagaimana menerapkan logika query yang digunakan pada database Fuzzy model Tahani secara komputerisasi melalui sistem berbasis web?</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-dark text-white">
        <div class="container text-center">
            <h2 class="mb-4">Temukan Pilihan Anda!</h2>
            <a class="btn btn-light btn-xl sr-button" href="/find">Find Now!</a>
        </div>
    </section>

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <h2 class="section-heading">About</h2>
                    <hr class="my-4">
                    <p class="mb-5">Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 ml-auto text-center">
                    <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                    <p>123-456-6789</p>
                </div>
                <div class="col-lg-4 mr-auto text-center">
                    <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                    <p>
                        <a href="mailto:your-email@your-domain.com">feedback@mail.com</a>
                    </p>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
@endsection