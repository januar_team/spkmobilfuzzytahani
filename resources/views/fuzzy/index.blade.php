@extends('layouts.user')

@section('content')
    <header class="masthead page text-center text-white d-flex">

    </header>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Temukan Kendaraan Yang Sesuai Dengan Anda</h2>
                    <hr class="my-4">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Kriteria</h3>
                    <form method="post" action="/find#result">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga</label>
                                    <select class="form-control" id="harga" name="harga" placeholder="">
                                        <option value="" {{(old('harga') == "") ? "selected" : ""}}>All</option>
                                        @foreach($harga as $item)
                                            <option value="{{$item->id}}" {{(old('harga') == $item->id) ? "selected" : ""}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Tahun</label>
                                    <select class="form-control" id="tahun" name="tahun" placeholder="">
                                        <option value="" {{(old('tahun') == "") ? "selected" : ""}}>All</option>
                                        @foreach($tahun as $item)
                                            <option value="{{$item->id}}" {{(old('tahun') == $item->id) ? "selected" : ""}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Jarak Tempuh</label>
                                    <select class="form-control" id="jarak_tempuh" name="jarak_tempuh" placeholder="">
                                        <option value="" {{(old('jarak_tempuh') == "") ? "selected" : ""}}>All</option>
                                        @foreach($jarak_tempuh as $item)
                                            <option value="{{$item->id}}" {{(old('jarak_tempuh') == $item->id) ? "selected" : ""}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kapasitas Mesin</label>
                                    <select class="form-control" id="kapasitas_mesin" name="kapasitas_mesin" placeholder="">
                                        <option value="" {{(old('kapasitas_mesin') == "") ? "selected" : ""}}>All</option>
                                        @foreach($kapasitas_mesin as $item)
                                            <option value="{{$item->id}}" {{(old('kapasitas_mesin') == $item->id) ? "selected" : ""}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Kapasitas Tangki</label>
                                    <select class="form-control" id="kapasitas_tangki" name="kapasitas_tangki" placeholder="">
                                        <option value="" {{(old('kapasitas_tangki') == "") ? "selected" : ""}}>All</option>
                                        @foreach($kapasitas_tangki as $item)
                                            <option value="{{$item->id}}" {{(old('kapasitas_tangki') == $item->id) ? "selected" : ""}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Torsi Maks</label>
                                    <select class="form-control" id="torsi" name="torsi" placeholder="">
                                        <option value="" {{(old('torsi') == "") ? "selected" : ""}}>All</option>
                                        @foreach($torsi as $item)
                                            <option value="{{$item->id}}" {{(old('harga') == $item->id) ? "selected" : ""}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kapasistas Penumpang</label>
                                    <select class="form-control" id="penumpang" name="penumpang" placeholder="">
                                        <option value="" {{(old('penumpang') == "") ? "selected" : ""}}>All</option>
                                        @foreach($penumpang as $item)
                                            <option value="{{$item->id}}" {{(old('penumpang') == $item->id) ? "selected" : ""}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Bahan Bakar</label>
                                    <select class="form-control" id="bahan_bakar" name="bahan_bakar" placeholder="Bahan bakar">
                                        <option value="" {{(old('bahan_bakar') == "") ? "selected" : ""}}>All</option>
                                        <option value="premium" {{(old('bahan_bakar') == "premium") ? "selected" : ""}}>premium</option>
                                        <option value="pertalite" {{(old('bahan_bakar') == "pertalite") ? "selected" : ""}}>pertalite</option>
                                        <option value="pertamax" {{(old('bahan_bakar') == "pertamax") ? "selected" : ""}}>pertamax</option>
                                        <option value="pertamax plus" {{(old('bahan_bakar') == "pertamax") ? "selected" : ""}}>pertamax plus</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Transmisi</label>
                                    <select class="form-control" id="transmisi" name="transmisi" placeholder="Transmisi">
                                        <option value="" {{(old('transmisi') == "") ? "selected" : ""}}>All</option>
                                        <option value="manual" {{(old('transmisi') == "manual") ? "selected" : ""}}>manual</option>
                                        <option value="automatic" {{(old('transmisi') == "automatic") ? "selected" : ""}}>automatic</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                @if(isset($variable))
                <div id="result" class="col-md-12" style="margin-top : 40px">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Merek</th>
                                @foreach($variable as $var => $val)
                                    <th>{{"$var($val->name)"}}</th>
                                @endforeach
                                <th>Fire Strength</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @foreach($results as $result)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$result->car->nama}}</td>
                                    <td>{{$result->car->merek}}</td>
                                    @foreach($variable as $var => $item)
                                        <?php $label = str_replace(" ", "_", strtolower($var)) ?>
                                        <td>{{ $result->{$label} }}</td>
                                    @endforeach
                                    <td>{{$result->fire_strength}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#mainNav').addClass('navbar-shrink');
        })
    </script>
@endsection