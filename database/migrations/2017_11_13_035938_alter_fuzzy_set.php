<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFuzzySet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fuzzy_sets', function (Blueprint $table) {
            $table->double('min_val');
            $table->double('mid_val');
            $table->double('max_val');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fuzzy_sets', function (Blueprint $table) {
            //
        });
    }
}
