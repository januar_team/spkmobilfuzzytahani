<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuzzySetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuzzy_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fuzzy_variable_id')->unsigned();
            $table->string('name');
            $table->integer('min_val');
            $table->integer('mid_val');
            $table->integer('max_val');

            $table->foreign('fuzzy_variable_id')->references('id')->on('fuzzy_variables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuzzy_sets');
    }
}
