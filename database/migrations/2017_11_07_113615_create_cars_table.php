<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merek');
            $table->string('nama');
            $table->integer('harga');
            $table->integer('tahun');
            $table->integer('jarak_tempuh');
            $table->integer('kapasitas_mesin');
            $table->integer('kapasitas_tangki');
            $table->double('torsi_maks');
            $table->integer('kapasitas_penumpang');
            $table->string('bahan_bakar');
            $table->string('transmisi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
