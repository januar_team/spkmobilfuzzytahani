<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::match(['get', 'post'], '/find', 'FuzzyController@index');

Route::middleware(['auth'])->prefix('admin')->namespace('Admin')->group(function () {
    Route::get('/', 'HomeController@index');
    Route::match(['get', 'post'], '/cars/index', 'CarsController@index');
    Route::post('/cars/add', 'CarsController@add');
    Route::post('/cars/edit/{id}', 'CarsController@edit');
    Route::post('/cars/delete', 'CarsController@delete');

    Route::get('/fuzzy/index', 'FuzzyController@index');
    Route::match(['get', 'post'], '/fuzzy/himpunan/{id}', 'FuzzyController@himpunan');
    Route::post('/fuzzy/add_fuzzyset/{id}', 'FuzzyController@add_fuzzyset');
    Route::post('/fuzzy/edit_fuzzyset/{id}', 'FuzzyController@edit_fuzzyset');
    Route::post('/fuzzy/delete_fuzzyset', 'FuzzyController@delete_fuzzyset');
});