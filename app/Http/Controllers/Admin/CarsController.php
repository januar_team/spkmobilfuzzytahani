<?php

namespace App\Http\Controllers\Admin;

use App\Model\Car;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarsController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            $search = $request->search['value'];
            $data = Car::where('nama','LIKE',"%$search%")
            ->orWhere('merek', 'LIKE', "%$search%");

            return datatables()->eloquent($data)->make(true);
        }
        return $this->view();
    }

    public function add(Request $request){
        $car = new Car();
        $car->fill($request->all());
        $car->save();
        return response()->json(['status' => true]);
    }

    public function edit(Request $request, $id){
        $car = Car::find($id);
        $car->fill($request->all());
        $car->save();
        return response()->json(['status' => true]);
    }

    public function delete(Request $request){
        $car = Car::find($request->id);
        $car->delete();
        return response()->json(['status' => true]);
    }
}
