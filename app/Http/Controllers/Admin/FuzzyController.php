<?php

namespace App\Http\Controllers\Admin;

use App\Model\FuzzySet;
use App\Model\FuzzyVariable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FuzzyController extends Controller
{
    public function index(Request $request){
        $data = FuzzyVariable::all();
        return $this->view(['data' => $data]);
    }

    public function himpunan(Request $request, $id){
        $variable = FuzzyVariable::find($id);
        if (!$variable)
            return redirect('/admin/fuzzy/index');

        if ($request->isMethod('post')){
            $fuzzy_set = FuzzySet::where('fuzzy_variable_id', '=', $id)
                ->orderBy('min_val', 'ASC')
                ->paginate(50);

            $response = collect([
                'draw' => intval($request->draw),
                'recordsTotal' => $fuzzy_set->total(),
                'recordsFiltered' => $fuzzy_set->total()
            ]);

            return response()->json($response->merge($fuzzy_set));
        }

        return $this->view(['variable' => $variable]);
    }

    public function add_fuzzyset(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'min_val' => 'required|numeric',
            'mid_val' => 'required|numeric',
            'max_val' => 'required|numeric'
        ]);

        $variable = FuzzyVariable::find($id);
        if (!$variable)
            return response()->json(['status' => false, 'message' => 'Variable not found']);

        $fuzzyset = new FuzzySet();
        $fuzzyset->fill($request->all());
        $fuzzyset->fuzzy_variable_id = $id;
        $fuzzyset->save();

        return response()->json(['status' => true]);
    }

    public function edit_fuzzyset(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'min_val' => 'required|numeric',
            'mid_val' => 'required|integer',
            'max_val' => 'required|integer'
        ]);

        $fuzzyset = FuzzySet::find($id);
        if (!$fuzzyset)
            return response()->json(['status' => false, 'message' => 'Item not found']);

        $fuzzyset->fill($request->all());
        $fuzzyset->save();

        return response()->json(['status' => true]);
    }

    public function delete_fuzzyset(Request $request){
        $fuzzy_set = FuzzySet::find($request->id);
        $fuzzy_set->delete();
        return response()->json(['status' => true]);
    }
}
