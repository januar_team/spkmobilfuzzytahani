<?php

namespace App\Http\Controllers;

use App\Model\Car;
use App\Model\FuzzySet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FuzzyController extends Controller
{
    public function index(Request $request){
        Input::flash();
        $data = [];
        if ($request->isMethod('post')){
            $variable = [];
            if (isset($request->harga)) $variable['Harga'] = FuzzySet::find($request->harga);
            if (isset($request->tahun)) $variable['Tahun'] = FuzzySet::find($request->tahun);
            if (isset($request->jarak_tempuh)) $variable['Jarak Tempuh'] = FuzzySet::find($request->jarak_tempuh);
            if (isset($request->kapasitas_mesin)) $variable['Kapasitas Mesin'] = FuzzySet::find($request->kapasitas_mesin);
            if (isset($request->kapasitas_tangki)) $variable['Kapasitas Tangki'] = FuzzySet::find($request->kapasitas_tangki);
            if (isset($request->torsi)) $variable['Torsi'] = FuzzySet::find($request->torsi);
            if (isset($request->penumpang)) $variable['Jumlah Penumpang'] = FuzzySet::find($request->penumpang);
            $data['variable'] = $variable;

            $cars = Car::all();
            $results = [];
            foreach ($cars as $car) {
                $obj = new \stdClass();
                $obj->car = $car;
                $obj->fire_strength = 1;
                foreach ($variable as $var => $item) {
                    $label = str_replace(" ", "_", strtolower($var));
                    if ($item->min_val == 0){
                        if ($car->{$label} <= $item->mid_val) {
                            $obj->{$label} = 1;
                        }else if($car->{$label} >= $item->max_val){
                            $obj->{$label} = 0;
                        }else{
                            $obj->{$label} = ($item->max_val - $car->{$label}) / ($car->{$label} - $item->mid_val);
                        }
                    }else if ($item->max_val == 0) {
                        if ($car->{$label} <= $item->min_val) {
                            $obj->{$label} = 0;
                        } else if ($car->{$label} >= $item->max_val) {
                            $obj->{$label} = 1;
                        } else {
                            $obj->{$label} = ($item->max_val - $car->{$label}) / ($car->{$label} - $item->mid_val);
                        }
                    }else if ($car->{$label} <= $item->min_val || $car->{$label} >= $item->max_val){
                        $obj->{$label} = 0;
                    }else if($car->{$label} <= $item->mid_val){
                        $obj->{$label} = ($car->{$label} - $item->min_val) / ($item->mid_val - $item->min_val);
                    }else{
                        $obj->{$label} = ($item->max_val - $car->{$label}) / ($item->max_val - $item->mid_val);
                    }

                    $obj->fire_strength = ($obj->{$label} < $obj->fire_strength) ? $obj->{$label} : $obj->fire_strength;
                }

                $results[] = $obj;
            }

            $data['results'] = $this->sort($results);
        }

        $harga = FuzzySet::where('fuzzy_variable_id', '=', 1)
            ->get();
        $tahun = FuzzySet::where('fuzzy_variable_id', '=', 2)
            ->get();
        $jarak_tempuh = FuzzySet::where('fuzzy_variable_id', '=', 3)
            ->get();
        $kapasitas_mesin = FuzzySet::where('fuzzy_variable_id', '=', 4)
            ->get();
        $kapasitas_tangki = FuzzySet::where('fuzzy_variable_id', '=', 5)
            ->get();
        $torsi = FuzzySet::where('fuzzy_variable_id', '=', 6)
            ->get();
        $penumpang = FuzzySet::where('fuzzy_variable_id', '=', 7)
            ->get();

        $data = array_merge($data, [
            'harga' => $harga,
            'tahun' => $tahun,
            'jarak_tempuh' => $jarak_tempuh,
            'kapasitas_mesin' => $kapasitas_mesin,
            'kapasitas_tangki' => $kapasitas_tangki,
            'torsi' => $torsi,
            'penumpang' => $penumpang,
        ]);
        return $this->view($data);
    }

    private function sort($array){
        $length = count($array);
        for ($j = 0; $j < $length-1; $j++) {
            $iMin = $j;
            for ($i = $j+1; $i < $length; $i++) {
                if ($array[$i]->fire_strength > $array[$iMin]->fire_strength) {
                    $iMin = $i;
                }
            }
            if ($iMin != $j) {
                // swap
                $temp = $array[$j];
                $array[$j] = $array[$iMin];
                $array[$iMin] = $temp;
            }
        }
        return $array;
    }
}
