<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FuzzySet extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'min_val', 'mid_val', 'max_val'];
}
