<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public $timestamps = false;
    protected $fillable = ['merek','nama','harga','tahun','jarak_tempuh','kapasitas_mesin','kapasitas_tangki',
        'torsi_maks', 'kapasitas_penumpang','bahan_bakar','transmisi'];
}
