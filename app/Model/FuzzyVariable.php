<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FuzzyVariable extends Model
{
    public $timestamps = false;
}
