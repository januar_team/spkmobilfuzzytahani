jQuery(document).ready(function ($) {
    var table = $('#datatable').DataTable({
        "bFilter": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "lengthChange": false,
        "ajax": {
            "url": "/admin/fuzzy/himpunan/" + $('#datatable').data('id'),
            "type": "POST",
            "data": {}
        },
        "columns": [
            {"data": "name"},
            {"data": "min_val"},
            {"data": "mid_val"},
            {"data": "max_val"},
            {
                render: function (data, type, row) {
                    return '<a href="#" class="badge bg-blue btn-edit"><i class="fa fa-pencil"></i> edit</a>' +
                        '<a href="#" class="badge bg-red btn-delete"><i class="fa fa-trash"></i> delete</a>';
                }
            }
        ],
        "aoColumnDefs": [{
            "aTargets": [0],
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
            }
        }],
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            $(nRow).attr('data', JSON.stringify(aData));
        }
    });

    $('#btn-add').click(function (event) {
        event.preventDefault();
        $('#modal-add').modal();
        $('#frm-add input').val('');
        $('#frm-add').attr('action', '/admin/fuzzy/add_fuzzyset/'+ $('#datatable').data('id'));
        $('#form-title').html('Tambah Himpunan Fuzzy');
    });

    $('#frm-add').submit(function (event) {
        event.preventDefault();
        var data = $(this).serialize();
        $('.alert').hide();
        $('#frm-add .btn-submit').button('loading');
        $.ajax({
            url: $('#frm-add').attr('action'),
            data: data,
            method: 'post',
            dataType: 'json'
        }).done(function (response) {
            if (response.status) {
                $('#modal-add').modal('toggle');
                $('.alert').hide();
                $('#frm-add input').val('');
                table.draw();
            } else {
                $('.alert').show();
                $('.alert span').html(response.message)
            }
        }).fail(function (xhr) {
            $('.form-group').addClass('has-error');
            if (xhr.status == 422){
                $('.form-group div').append('<span class="help-block">'+ xhr.responseJSON.commodity[0]+'</span>')
            }else{
                $('.alert').show();
                $('.alert span').html('Failed to saving data');
            }
        }).always(function () {
            $('#frm-add .btn-submit').button('reset');
        });
    });

    $('#datatable').on('click', '.btn-edit', function (event) {
        event.preventDefault();
        var data = JSON.parse($(this).parent().parent().attr('data'));
        for (var property in data){
            if (property == 'bahan_bakar ' || property == 'transmisi'){
                $('#frm-add select#'+property).val(data[property]);
            }else{
                $('#frm-add input#'+property).val(data[property]);
            }
        }

        $('#modal-add').modal();
        $('#frm-add').attr('action', '/admin/fuzzy/edit_fuzzyset/' + data.id);
        $('#form-title').html('Edit Himpunan Fuzzy');
    });

    $('#datatable').on('click', '.btn-delete', function (event) {
        event.preventDefault();
        var data = JSON.parse($(this).parent().parent().attr('data'));
        $('#modal-delete').modal();
        $('#frm-delete input#id').val(data.id);
    });

    $('#frm-delete').submit(function (event) {
        event.preventDefault();
        var data = $(this).serialize();
        $('#frm-delete .btn-submit').button('loading');
        $.ajax({
            url: '/admin/fuzzy/delete_fuzzyset',
            data: data,
            method: 'post',
            dataType: 'json'
        }).done(function (response) {
            if (response.status) {
                $('#modal-delete').modal('toggle');
                $('#frm-delete input').val('');
                table.draw();
            }
        }).fail(function (xhr) {
            console.log(xhr);
        }).always(function () {
            $('#frm-delete .btn-submit').button('reset');
        });
    });
});