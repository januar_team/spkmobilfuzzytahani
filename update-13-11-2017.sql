ALTER TABLE `fuzzy_sets`
  CHANGE `min_val` `min_val` DOUBLE NOT NULL,
  CHANGE `mid_val` `mid_val` DOUBLE NOT NULL,
  CHANGE `max_val` `max_val` DOUBLE NOT NULL;
